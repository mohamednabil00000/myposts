package com.MyPosts.dto;

import com.MyPosts.entity.ApplicationUser;
import com.MyPosts.view.PostView;
import com.fasterxml.jackson.annotation.JsonView;

import java.util.Date;

public class PostDto {
    @JsonView(PostView.Public.class)
    private long id;
    @JsonView(PostView.Public.class)
    private String content;
    @JsonView(PostView.Public.class)
    private String status;
    @JsonView(PostView.Public.class)
    private Date created_at;
    @JsonView(PostView.Public.class)
    private ApplicationUser postCreator;

    public PostDto(long id, String content, String status, Date created_at, ApplicationUser postCreator) {
        this.id = id;
        this.content = content;
        this.status = status;
        this.created_at = created_at;
        this.postCreator = postCreator;
    }

    public PostDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ApplicationUser getPostCreator() {
        return postCreator;
    }

    public void setPostCreator(ApplicationUser postCreator) {
        this.postCreator = postCreator;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }
}
