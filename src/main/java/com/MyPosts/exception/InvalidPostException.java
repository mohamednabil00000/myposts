package com.MyPosts.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidPostException extends RuntimeException {
    public InvalidPostException() {
        super();
    }

    public InvalidPostException(String message){
        super(message);
    }
}
