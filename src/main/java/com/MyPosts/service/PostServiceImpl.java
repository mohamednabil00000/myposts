package com.MyPosts.service;

import com.MyPosts.dao.PostRepository;
import com.MyPosts.dao.UserRepository;
import com.MyPosts.entity.ApplicationUser;
import com.MyPosts.entity.Post;
import com.MyPosts.helper.Status;
import com.MyPosts.specification.PostSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class PostServiceImpl implements PostService {
    @Autowired
    private UserRepository applicationUserRepository;
    @Autowired
    private PostRepository postRepository;

    @Override
    public Post create(Post post, String username){
        ApplicationUser postCreator = applicationUserRepository.findByUsername(username);
        post.setPostCreator(postCreator);
        return postRepository.save(post);
    }
    @Override
    public List<Post> search(String text){
        List<String> words = Arrays.asList(text.split(" "));
        List<Post> matches = postRepository.findAll(PostSpecification.publicPosts().
                                                    and(PostSpecification.forWords(words)
                                                       )
                                                   );
        return matches;
    }
}
