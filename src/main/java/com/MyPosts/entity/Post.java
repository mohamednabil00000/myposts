package com.MyPosts.entity;

import com.MyPosts.helper.Status;
import com.MyPosts.view.PostView;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;

@Entity
public class Post extends BaseEntity {

    @JsonView(PostView.Public.class)
    private String content;
    @Enumerated(EnumType.ORDINAL)
    @JsonView(PostView.Public.class)
    private Status status;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "post_creator_id", nullable = false)
    @JsonView(PostView.Public.class)
    private ApplicationUser postCreator;

    public Post(String content, Status status, ApplicationUser postCreator) {
        this.content = content;
        this.status = status;
        this.postCreator = postCreator;
    }

    public Post(){

    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ApplicationUser getPostCreator() {
        return postCreator;
    }

    public void setPostCreator(ApplicationUser postCreator) {
        this.postCreator = postCreator;
    }
}
