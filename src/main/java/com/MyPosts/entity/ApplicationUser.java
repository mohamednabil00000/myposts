package com.MyPosts.entity;
import com.MyPosts.view.PostView;
import com.MyPosts.view.UserView;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
public class ApplicationUser extends BaseEntity{

    @JsonView({UserView.Public.class,PostView.Public.class})
    private String username;
    @JsonView({UserView.Public.class,PostView.Public.class})
    private String fullname;
    private String password;
    @OneToMany(mappedBy = "postCreator", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<Post> posts;
    public ApplicationUser() {
    }

    public ApplicationUser(String username, String password, String fullname) {
        this.username = username;
        this.password = password;
        this.fullname = fullname;
    }

    public ApplicationUser(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    @JsonIgnore
    public Set<Post> getPosts() {
        return posts;
    }

    public void setPosts(Set<Post> posts) {
        this.posts = posts;
    }
}