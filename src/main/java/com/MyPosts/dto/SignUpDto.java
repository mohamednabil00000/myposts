package com.MyPosts.dto;

import javax.validation.constraints.NotNull;

public class SignUpDto {
    private long id;
    @NotNull
    private String username;

    @NotNull
    private String fullname;

    public SignUpDto() {}

    public SignUpDto(long id, @NotNull String username, @NotNull String fullname) {
        this.id = id;
        this.username = username;
        this.fullname = fullname;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
}