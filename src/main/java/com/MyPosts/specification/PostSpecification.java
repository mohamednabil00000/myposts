package com.MyPosts.specification;

import com.MyPosts.entity.Post;
import com.MyPosts.helper.Status;
import org.springframework.data.jpa.domain.Specification;

import java.util.Collection;


public class PostSpecification {

    public static Specification<Post> publicPosts() {
        return (root, query, builder) ->{
            return builder.equal(root.get("status"), Status.PUBLIC);
        };
    }

    public static Specification<Post> forWords(Collection<String> words) {
        if(words == null || words.isEmpty())
            throw new RuntimeException("List of words cannot be empty.");

        return (root, query, builder) -> words.stream()
                .map(String::toLowerCase)
                .map(word -> "%" + word + "%")
                .map(word -> builder.like(builder.lower(root.get("content")), word))
                .reduce(builder::or)
                .get();
    }
}
