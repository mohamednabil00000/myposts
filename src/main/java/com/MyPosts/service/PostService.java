package com.MyPosts.service;

import com.MyPosts.entity.Post;

import java.util.List;

public interface PostService {
    Post create(Post post, String username);
    List<Post> search(String text);
}
