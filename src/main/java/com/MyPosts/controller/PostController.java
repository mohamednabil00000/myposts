package com.MyPosts.controller;

import com.MyPosts.dto.PostDto;
import com.MyPosts.entity.Post;
import com.MyPosts.exception.InvalidPostException;
import com.MyPosts.service.PostService;
import com.MyPosts.view.PostView;
import com.fasterxml.jackson.annotation.JsonView;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/posts")
public class PostController {

    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private PostService postService;


    @JsonView(PostView.Public.class)
    @PostMapping
    public PostDto create(Authentication authentication, @RequestBody Post post){
        String username = authentication.getName();
        Post newPost =  postService.create(post, username);
        return convertToDto(newPost);
    }
    @JsonView(PostView.Public.class)
    @GetMapping
    public List<PostDto> search(@RequestParam String text){ //search for any text in public posts content
        if(text == null){
            throw new InvalidPostException("text should not be null");
        }
        if(text.trim().length() == 0){ //remove un-necessary spaces
            throw new InvalidPostException("text should not be empty");
        }
        List<Post> posts =  postService.search(text);
        return convertToDto(posts);
    }

    private PostDto convertToDto(Post post) {
        return modelMapper.map(post, PostDto.class);
    }

    private List<PostDto> convertToDto(List<Post> posts) {
        List<PostDto> dtoPosts = new ArrayList<>();
        for(Post post: posts)
            dtoPosts.add(convertToDto(post));
        return dtoPosts;
    }
}
